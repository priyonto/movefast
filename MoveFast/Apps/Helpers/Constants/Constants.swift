//
//  Constants.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


//MARK:- COLORS

let parentBGColor: UIColor = UIColor(hexString: "#23272F")
let navigationBarTitleColor: UIColor = UIColor(hexString: "#C1C1C1")

// MARK:- Line Spacing For Collections

let lineSpacing: CGFloat = 12
