//
//  APIService.swift
//  MoveFast
//
//  Created by Priyo on 21/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import Foundation

/// Class to handle all kind of requests

class APIService {
    
    public static let shared = APIService()
    private init() {}
    private let urlSession = URLSession.shared
    private let baseURL = URL(string: "https://api.unsplash.com/")!
    private let clientId: String = "yQaaLgngUTX8eS_cV0cpaZA3IXO5R9x9e7pD-YiNREU"
    
    
    
    // Enum Endpoint
    enum Endpoint: String, CustomStringConvertible, CaseIterable {
        
        case photos

        var description: String {
            switch self {
                
            case .photos:
                
                return "photos/"

            }
            
            
        }
        
    }
    
    
}


// MARK:- EXTENSION TO CONTAIN FUNCTIONS OF DATA FETCH FROM DIFFERENT API'S

extension APIService {

    public func fetchPhotos(from endpoint: Endpoint, page: Int, perPage: Int, orderBy: PhotosOrder , result: @escaping ([Photo]?, Error?) -> Void) {
        let mainUrl = baseURL
            .appendingPathComponent(endpoint.description)
        
        guard var urlComponents = URLComponents(url: mainUrl, resolvingAgainstBaseURL: true) else {
            return
        }
        
        let queryItems = [URLQueryItem(name: "client_id", value: clientId), URLQueryItem(name: "page", value: page.toString()),URLQueryItem(name: "per_page", value: perPage.toString()), URLQueryItem(name: "order_by", value: orderBy.rawValue)]
        urlComponents.queryItems = queryItems
        
        
        guard let url = urlComponents.url else {
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        fetchDataFromRemoteServer(urlRequest: urlRequest, completion: result)
    }
    
}


// MARK:- GENERIC FUNCTION TO FETCH DATA FROM REMOTE SERVER

extension APIService {
    
    private func fetchDataFromRemoteServer<T: Decodable>(urlRequest: URLRequest, completion: @escaping (T?, Error?) -> Void) {
        
   
        URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
            
            
            if let err = err {
      
                completion(nil, err)
                return
            }
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects, nil)
                
            } catch {
                
                print(error)
                completion(nil, error)
            }
        }.resume()
    }
    

}


