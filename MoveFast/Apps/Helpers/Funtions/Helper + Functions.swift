//
//  Helper + Functions.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


// MARK:- CREATE NAVIGATION CONTROLLER

public func createNavigationController(controller: UIViewController, title: String) -> UIViewController {
    
    let navigationController = UINavigationController(rootViewController: controller)
    navigationController.tabBarItem.title = title
    navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.Roboto(.Regular, size: 16) ,.foregroundColor: navigationBarTitleColor]
    navigationController.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.Roboto(.Bold, size: 30) ,.foregroundColor: navigationBarTitleColor]
    navigationController.navigationBar.prefersLargeTitles = true
    navigationController.navigationBar.barTintColor = parentBGColor
    navigationController.navigationBar.isTranslucent = false
    navigationController.view.backgroundColor = parentBGColor
    controller.navigationItem.title = title
    controller.view.backgroundColor = .white
    
    return navigationController
}
