//
//  Int + Extension.swift
//  MoveFast
//
//  Created by Priyo on 21/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


extension Int {
    
    public func toString() -> String {
        
        return String.init(describing: self)
        
    }
    
}
