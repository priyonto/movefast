//
//  UIImageView + Extension.swift
//  MoveFast
//
//  Created by Priyo on 22/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

extension UIImageView {
    
    // Data holder tap recognizer
    private class TapWithDataRecognizer:UITapGestureRecognizer {
        var from:UIViewController?
        var photoDatasource:PhotoDataSource?
        var initialIndex:Int = 0
    }
    
    private var vc:UIViewController? {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController
            else { return nil }
        return rootVC.presentedViewController != nil ? rootVC.presentedViewController : rootVC
    }
    
    
    
    public func setupImageViewer(
        
        photos:[Photo],
        initialIndex:Int = 0,
        placeholder: UIImage? = nil,
        from:UIViewController? = nil) {
        
        let dataSource = SimplePhotoDataSource(photos: photos)
        
        setup(
            datasource: dataSource,
            initialIndex: initialIndex,
            from: from)
    }

    private func setup (
        
        datasource:PhotoDataSource?,
        initialIndex:Int = 0,
        from: UIViewController? = nil) {
        
        var _tapRecognizer:TapWithDataRecognizer?
        gestureRecognizers?.forEach {
            if let _tr = $0 as? TapWithDataRecognizer {
                // if found, just use existing
                _tapRecognizer = _tr
            }
        }
        
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFill
        clipsToBounds = true
        
        if _tapRecognizer == nil {
            _tapRecognizer = TapWithDataRecognizer(
                target: self, action: #selector(showImageViewer(_:)))
            _tapRecognizer!.numberOfTouchesRequired = 1
            _tapRecognizer!.numberOfTapsRequired = 1
        }
        // Pass the Data
        _tapRecognizer!.photoDatasource = datasource
        _tapRecognizer!.initialIndex = initialIndex
        _tapRecognizer!.from = from
        addGestureRecognizer(_tapRecognizer!)
    }
    
    @objc
    private func showImageViewer(_ sender:TapWithDataRecognizer) {
        guard let sourceView = sender.view as? UIImageView else { return }
        
        let imageCarousel = ImageCarouselViewController.create(
            sourceView: sourceView,
            photoDataSource: sender.photoDatasource,
            initialIndex: sender.initialIndex)
        let presentFromVC = sender.from ?? vc
        presentFromVC?.present(imageCarousel, animated: false, completion: nil)
    }
}

