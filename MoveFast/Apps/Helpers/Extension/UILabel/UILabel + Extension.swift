//
//  UILabel + Extension.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


extension UILabel {
    
    convenience init(text: String, font: UIFont, color: UIColor, numberOfLines: Int = 1) {
        self.init(frame: .zero)
        self.text = text
        self.textColor = color
        self.font = font
        self.numberOfLines = numberOfLines
    }
}
