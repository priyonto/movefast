//
//  VisualEffectView + Extension.swift
//  MoveFast
//
//  Created by Priyo on 23/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

extension UIVisualEffectView {
    
    
    /// It will take corners (array of CACornerMask) as argument
    
    convenience init(corners: [CACornerMask]) {
        self.init(frame: .zero)
        
        let blurEffect = UIBlurEffect(style: .dark)
        self.effect = blurEffect
        self.clipsToBounds = true
        self.layer.maskedCorners = .init(corners)
        
    }
}
