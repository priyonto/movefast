//
//  UIFont + Extension.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

extension UIFont {
    
    public enum RobotoType: String {

        
        case BlackItalic = "-BlackItalic"
        case Black = "-Black"
        case BoldItalic = "-BoldItalic"
        case Bold = "-Bold"
        case LightItalic = "-LightItalic"
        case Light = "-Light"
        case MediumItalic = "-MediumItalic"
        case Medium = "-Medium"
        case Regular = "-Regular"
        case Italic = "-Italic"
        case ThinItalic = "-ThinItalic"
        case Thin = "-Thin"
        
        
    }
    
    static func Roboto(_ type: RobotoType = .Regular, size: CGFloat = UIFont.systemFontSize) -> UIFont {
        return UIFont(name: "Roboto\(type.rawValue)", size: size)!
    }
    
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
}

