//
//  UIView + Extension.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

extension UIView {
    
    /// It will take color and corner radius for view as argument
    /// To reduce writing up the same codes for view declaration of this type
    
    convenience init(color: UIColor, cornerRadius: CGFloat = 0 ) {
        self.init(frame: .zero)
        
        self.backgroundColor = color
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
}
