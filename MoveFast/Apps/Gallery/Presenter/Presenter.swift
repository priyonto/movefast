//
//  Presenter.swift
//  MoveFast
//
//  Created by Priyo on 23/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

/// Description
/// This class gets the logics from view and prepare contents for display and for reacting to user interactions


/// GalleryViewToPresenterProtocol is the bridge from View To Presenter
class GalleryPresenter: GalleryViewToPresenterProtocol {
    
    
    
    var view: GalleryPresenterToViewProtocol?
    var interactor: GalleryPresenterToInteractorProtocol?
    var router: GalleryPresenterToRouterProtocol?

    
    /// Starts data fetch request.
    /// Takes an argument of itemPerPage, pageNumber and order of photos
    /// Tells interactor to make the requests
    
    func startDataFetchRequest(itemPerPage: Int, pageNumber: Int, order: PhotosOrder) {
        
        interactor?.datafetchRequest(itemPerPage: itemPerPage, pageNumber: pageNumber, order: order)
        
    }


}


/// GalleryInteractorToPresenterProtocol is the bridge from Interactor To Presenter
/// After requests been made, intereactor process the request and returns the response to Presenter
/// Then Presenter returns that response to View to show them to user
extension GalleryPresenter: GalleryInteractorToPresenterProtocol {
    
    func requestSuccess(photos: [Photo]) {
        
        view?.showSuccess(photos: photos)
    }
    
    func requestFailed(error: Error) {
        
        view?.showError(error: error)
    }

}
