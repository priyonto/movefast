//
//  Interactor.swift
//  MoveFast
//
//  Created by Priyo on 23/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

/// Description
/// This class handles all communication related to fetch data from remote server or cache
/// It make requests based on user requirements from view via presenter.
/// And returns the response to view via presenter.

class GalleryInteractor: GalleryPresenterToInteractorProtocol{

    
    var presenter: GalleryInteractorToPresenterProtocol?
    
    var photos: [Photo] = [] /// photo: It holds the photos data fetched from remote server
    
    /// Start Request
    
    func datafetchRequest(itemPerPage: Int, pageNumber: Int, order: PhotosOrder) {
        
        
        APIService.shared.fetchPhotos(from: .photos, page: pageNumber, perPage: itemPerPage, orderBy: order) { result, error in
            
            if let error = error {

                self.presenter?.requestFailed(error: error)
                return
            }
            
            self.photos += result ?? []
            self.presenter?.requestSuccess(photos: self.photos)
            
        }
        
    }
    

    
}
