//
//  SimpleImageDataSource.swift
//  MoveFast
//
//  Created by Priyo on 22/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//


public protocol PhotoDataSource:class {
    func numberOfPhotos() -> Int
    func photoItem(at index:Int) -> Photo
}


class SimplePhotoDataSource:PhotoDataSource {
    
    private(set) var photos:[Photo]
    
    init(photos: [Photo]) {
        self.photos = photos
    }
    
    func numberOfPhotos() -> Int {
        return photos.count
    }
    
    func photoItem(at index: Int) -> Photo {
        return photos[index]
    }
}
