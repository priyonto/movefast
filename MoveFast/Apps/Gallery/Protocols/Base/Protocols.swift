//
//  Protocols.swift
//  MoveFast
//
//  Created by Priyo on 23/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


/// Protocols to connect View to presenter
/// Start

protocol GalleryViewToPresenterProtocol: class{
    
    var view: GalleryPresenterToViewProtocol? {get set}
    var interactor: GalleryPresenterToInteractorProtocol? {get set}
    var router: GalleryPresenterToRouterProtocol? {get set}
    func startDataFetchRequest(itemPerPage: Int, pageNumber: Int, order: PhotosOrder)

}

/// End

/// Protocols to connect Presenter to View
/// Start

protocol GalleryPresenterToViewProtocol: class{
    func showSuccess(photos: [Photo])
    func showError(error: Error)

}

/// End

/// Protocols to connect Presenter to Router
/// Start

protocol GalleryPresenterToRouterProtocol: class {
    static func createModule()-> GalleryController
}

/// End


/// Protocols to connect Presenter to Interactor
/// Start

protocol GalleryPresenterToInteractorProtocol: class {
    var presenter: GalleryInteractorToPresenterProtocol? {get set}
    func datafetchRequest(itemPerPage: Int, pageNumber: Int, order: PhotosOrder)
}

/// End

/// Protocols to connect Intereactor to presenter
/// Start

protocol GalleryInteractorToPresenterProtocol: class {
    func requestSuccess(photos: [Photo])
    func requestFailed(error: Error)
}

/// End
