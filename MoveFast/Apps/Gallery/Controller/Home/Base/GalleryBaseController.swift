//
//  GalleryBaseController.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

/// Description
/// This UICollectionViewController class is super class of other UICollectionViewControllers  used all over the app
/// Other UICollectionViewControllers inherits from this class and it prohibits the duplication of init() method each time a UICollectionViewController created.

class GalleryBaseController: UICollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Base background color is set on the main container
        collectionView.backgroundColor = parentBGColor
    }
    
    init() {
        
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
