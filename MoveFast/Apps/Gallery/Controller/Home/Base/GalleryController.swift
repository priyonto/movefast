//
//  GalleryController.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit
import Network


class GalleryController: GalleryBaseController, UISearchBarDelegate {
    
    // MARK:- VARIABLES DECLARATION
    
    var photos : [Photo] = [] /// photos: It holds the photos fetched from remote server
    var unfilteredPhotos: [Photo] = [] /// unfilteredPhotos: It holds the photos fetched from remote server, it's unfiltered and will be used for search function
    var currentPage: Int = 1 /// Current page number
    var perPageItem: Int = 10 /// Number of items we are going to fetch from server on per page
    
    
    
    var searchController = UISearchController(searchResultsController: nil) /// Instance of UISearchController fro setting up searchbar on top of the page
    var timer: Timer? /// timer to fire up search action
    var isSearching: Bool = false /// A boolen variable to kepp track whether searching is active or not
    
    
    let monitor = NWPathMonitor() /// monitor:  Instance of NWPathMonitor to check network availability and network changes
    var presentor: GalleryViewToPresenterProtocol? /// presentor: Instance used to interact with view/interactor via protocol
    
    /// activityIndicatorView : Used to show loading animation while app is fetching data from remote server
    /// Start
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .whiteLarge)
        aiv.color = .black
        aiv.startAnimating()
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    /// End
    
    var isLayoutGrid: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI() /// Setup UI Components like background color, register cells etc.
        setupNetworkChangeMonitor() /// Setup network change monitor instantce
        setupSearchBar() /// Setup the search bar
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupActivitIndicator() /// Setup activity inidicator
  
    }
    
    
    
}

extension GalleryController {
    
    /// This method is to setup UI components data.
    /// Start
    
    fileprivate func setupUI() {
        
        collectionView.backgroundColor = parentBGColor /// Collectionview background color set to parentBGColor
        registerCell() /// Calls registerCells() method to register declared cells
        listButton() /// show list button in navigation bar right button as we will show the list layout at begin
        
    }
    
    /// This method is to setup grid button in navigation bar right button
    /// Start
    
    func gridButton() {

        let layoutButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_grid"), style: .done, target: self, action: #selector(layoutTapped(sender:)))
        self.navigationItem.rightBarButtonItem = layoutButton
    }
    /// End
    
    /// This method is to setup grid button in navigation bar right button
    /// Start
    
    func listButton() {
        
        let layoutButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_list"), style: .done, target: self, action: #selector(layoutTapped(sender:)))
        self.navigationItem.rightBarButtonItem = layoutButton
        
    }
    
    /// End
    
    /// Method to alter the photos layout based on user action
    
    @objc func layoutTapped(sender: UIBarButtonItem) {
        
        if isLayoutGrid {
            
            isLayoutGrid = false
            listButton()
            
        } else {
            
            isLayoutGrid = true
            gridButton()
        }
        
        UIView.transition(with: self.collectionView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            
            self.collectionView.reloadData()
            
        }, completion: nil)
    }
    
    
    /// End
    
    
    
    /// This method is to setup instance of NWPathMonitor
    fileprivate func setupNetworkChangeMonitor() {
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                

                /// If device connected to network (Wifi/Cellular/Other) the request will be made to fetch data from remote server.
                /// If device is not connected to network it will show alert
                /// If there is no cache data on device and network is not also connected it will show activity indicator.
                /// If device then connected to network layout will be reloaded with data from remote server.
                
                
                /// Request to fetch data from remote server
                self.presentor?.startDataFetchRequest(itemPerPage: self.perPageItem, pageNumber: self.currentPage, order: .latest)
  
                
            } else {
                
    
                DispatchQueue.main.async {
                    
                    /// A notification  will be showed if there is no connection available.
                    /// It will be showed at any time devices got disconncted.
                    
                    print("no internet")
                    
                }
                
            }
        }
        
        
        /// Network monitor queue started to check network changes.
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    /// This method set activity indicator
    fileprivate func setupActivitIndicator() {
        
        view.addSubview(activityIndicatorView) /// Added to view as subview
        activityIndicatorView.fillSuperview() /// Constraints to fill superview.
        
    }
    
    
    /// This method registers cell of collectionview
    
    fileprivate func registerCell() {
        
        
        collectionView.register(GalleryGridCell.self, forCellWithReuseIdentifier: CellType.grid.rawValue) /// Cell for grid layout
        collectionView.register(GalleryListCell.self, forCellWithReuseIdentifier: CellType.list.rawValue) /// Cell for list layout
        
    }
    
    
    
    
    /// This method set the search bar on top of collectionview with assigned properties
    
    fileprivate func setupSearchBar() {
        
        definesPresentationContext = true
        navigationItem.searchController = self.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search by name"
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([.foregroundColor : UIColor.lightGray], for: .normal)
    }
    
}

extension GalleryController {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        

        /// introduce some delay before performing the search
        /// If search text is nil the initially fetched photos will be shown, there is a little delay animation used for collectionview reload so that it reloads data smoothly.
        /// If serach text is not nil, this method will find match between Photo Description and Alt Description from entered text and will return the response
        /// And reload the collectionview with transition
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
            
            
            if searchText != "" {
                
                self.isSearching = true
                
                self.photos = self.unfilteredPhotos
                self.photos = self.photos.filter( {
                    
                    let description = $0.photoDescription ?? $0.altDescription ?? ""
                    let filtered = description.lowercased().contains(searchText.lowercased())
                    return filtered
                    
                })
                UIView.transition(with: self.collectionView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                }, completion: nil)
            }
                
            else {
                
                
                self.isSearching = false
                self.photos = self.unfilteredPhotos
                UIView.transition(with: self.collectionView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                }, completion: nil)
            }
            
        })
    }
}


// MARK:- EXTENTION FOR UICOLLECTIONVIEWCONTROLLER DELEGATE & DATA SOURCE

extension GalleryController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        /// Check if the search is active
        /// If active it will return and no request will be made to fetch more data
        /// If its not active, then make request
        
        guard !isSearching else {
            return
        }
        
        /// If scroll reaches the last item of fetched data
        /// Request for more data
        if indexPath.item == self.photos.count - 1 {
            
            /// Update the current page variable as soon as we reach to the end of current page
            self.currentPage += 1
            /// Request to fetch data from remote server
            self.presentor?.startDataFetchRequest(itemPerPage: self.perPageItem, pageNumber: self.currentPage, order: .latest)
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let photo = photos[indexPath.item]
        
        /// If the Grid Layout is selected, the grid type cell will be dequeued
        /// if the List Layout is selected, the list type cell will be dequeued
        
        if isLayoutGrid {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellType.grid.rawValue, for: indexPath)
            
            if let cell = cell as? GalleryGridCell {
                
                cell.photo = photo
                /// Setup the imageviewer helper controller with photos, initial index and placeholder
                cell.imageView.setupImageViewer(photos: self.photos, initialIndex: indexPath.item, placeholder: nil, from: self)
                
            }
            
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellType.list.rawValue, for: indexPath)
            
            if let cell = cell as? GalleryListCell {
                
                cell.photo = photo
                /// Setup the imageviewer helper controller with photos, initial index and placeholder
                cell.imageView.setupImageViewer(photos: self.photos, initialIndex: indexPath.item, placeholder: nil, from: self)
            }
            
            
            return cell
            
        }
        
    }

    
}

// MARK:- EXTENTION FOR UICOLLECTIONVIEWDELEGATEFLOWLAYOUT
/// Here the size of the base cells are manipulated.

extension GalleryController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if isLayoutGrid {
            
            let width = (collectionView.frame.size.width - lineSpacing * 4) / 3
            
            return .init(width: width, height: width)
            
        } else {
            
            let width = collectionView.frame.size.width - lineSpacing * 2
            let height: CGFloat = 250
            
            return .init(width: width, height: height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return .init(top: 8, left: lineSpacing, bottom: 8, right: lineSpacing)
    }
    
    
}


// MARK:- EXTENSION TO UPDATE UI WITH RESPONSE FROM PRESENTER WHICH INTERACTOR PASSED TO PRESENTER
/// Here we get the response from requests made earlier and populate them.

extension GalleryController: GalleryPresenterToViewProtocol {
    
    
    func showSuccess(photos: [Photo]) {
        
        self.photos = photos /// photos variable is populated
        self.unfilteredPhotos = photos
        
        DispatchQueue.main.async {
            
            self.activityIndicatorView.stopAnimating() /// hide activity indicator
            self.collectionView.reloadData() ///reload view with updated data
        }
 
    }
    
    
    
    func showError(error: Error) {
        
        /// this has no use case yet!
        print("Error: ", error)
    }
    
    
    
}
