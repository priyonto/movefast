//
//  ImageCarouselViewController.swift
//  MoveFast
//
//  Created by Priyo on 22/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit


public class ImageCarouselViewController: UIPageViewController {
    
    weak var photoDataSource:PhotoDataSource?
    var initialIndex = 0
    var sourceView:UIImageView!

    private(set) lazy var navBar:UINavigationBar = {
        let _navBar = UINavigationBar(frame: .zero)
        _navBar.isTranslucent = true
        _navBar.setBackgroundImage(UIImage(), for: .default)
        _navBar.shadowImage = UIImage()
        return _navBar
    }()
    
    private(set) lazy var backgroundView:UIView = {
        let _v = UIView()
        _v.backgroundColor = parentBGColor
        _v.alpha = 0.0
        return _v
    }()
    
    private(set) lazy var navItem = UINavigationItem()
    
    public static func create(
        
        sourceView:UIImageView,
        photoDataSource: PhotoDataSource?,
        initialIndex:Int = 0) -> ImageCarouselViewController {
        
        let pageOptions = [UIPageViewController.OptionsKey.interPageSpacing: 20]
        
        let imageCarousel = ImageCarouselViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal,
            options: pageOptions)
        
        imageCarousel.modalPresentationStyle = .overFullScreen
        imageCarousel.modalPresentationCapturesStatusBarAppearance = true
        
        imageCarousel.sourceView = sourceView
        imageCarousel.photoDataSource = photoDataSource
        imageCarousel.initialIndex = initialIndex
       
        return imageCarousel
    }
    
    private func addNavBar() {
        
        // Add Navigation Bar
        
        let closeBarButton = UIBarButtonItem (
            image: #imageLiteral(resourceName: "icon_close"),
            style: .plain,
            target: self,
            action: #selector(dismiss(_:)))
        
        navItem.leftBarButtonItem = closeBarButton
        navItem.leftBarButtonItem?.tintColor = .white
        navBar.alpha = 0.0
        navBar.items = [navItem]
        navBar.insert(to: view)
    }
    
    private func addBackgroundView() {
        view.addSubview(backgroundView)
        backgroundView.bindFrameToSuperview()
        view.sendSubviewToBack(backgroundView)
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        addBackgroundView()
        addNavBar()
  
        view.backgroundColor = .clear
        dataSource = self

        let initialVC = ImageViewerController(sourceView: sourceView)
        initialVC.index = initialIndex
        if let photoDataSource = photoDataSource {
            initialVC.photoItem = photoDataSource.photoItem(at: initialIndex)
        }
        initialVC.animateOnDidAppear = true
        initialVC.delegate = self
        setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.235) {
            self.navBar.alpha = 1.0
        }
    }
    
    @objc
    private func dismiss(_ sender:UIBarButtonItem) {
        dismissMe(completion: nil)
    }
    
    public func dismissMe(completion: (() -> Void)? = nil) {
        sourceView.alpha = 1.0
        UIView.animate(withDuration: 0.235, animations: {
            self.view.alpha = 0.0
        }) { _ in
            self.dismiss(animated: false, completion: completion)
        }
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension ImageCarouselViewController:UIPageViewControllerDataSource {
    public func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vc = viewController as? ImageViewerController else { return nil }
        guard let photoDatasource = photoDataSource else { return nil }
        guard vc.index > 0 else { return nil }
 
        let newIndex = vc.index - 1
        let sourceView = newIndex == initialIndex ? self.sourceView : nil
        return ImageViewerController.create(
            index: newIndex,
            photoItem:  photoDatasource.photoItem(at: newIndex),
            sourceView: sourceView,
            delegate: self)
    }
    
    public func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vc = viewController as? ImageViewerController else { return nil }
        guard let photoDatasource = photoDataSource else { return nil }
        guard vc.index <= (photoDataSource!.numberOfPhotos() - 2) else { return nil }
        
        let newIndex = vc.index + 1
        let sourceView = newIndex == initialIndex ? self.sourceView : nil
        return ImageViewerController.create(
            index: newIndex,
            photoItem:  photoDatasource.photoItem(at: newIndex),
            sourceView: sourceView,
            delegate: self)
    }
}

extension ImageCarouselViewController:ImageViewerControllerDelegate {
    func imageViewerDidClose(_ imageViewer: ImageViewerController) {
        sourceView.alpha = 1.0
    }
}
