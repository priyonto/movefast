//
//  Router.swift
//  MoveFast
//
//  Created by Priyo on 23/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

/// Description
/// This class acts as a router for GalleryController for all kind of interactions inbound and outbound

class GalleryRouter: GalleryPresenterToRouterProtocol {
    
    /// Create base controller module assigning all property: presenter, interector, router and view [VIPER Pattern used]
    
    static func createModule() -> GalleryController {
        
        let view = GalleryController() /// Instances of GalleryController
        
        let presenter: GalleryViewToPresenterProtocol & GalleryInteractorToPresenterProtocol = GalleryPresenter()
        let interactor: GalleryPresenterToInteractorProtocol = GalleryInteractor()
        let router: GalleryPresenterToRouterProtocol = GalleryRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view /// Returns GalleryController after assiging all property.
        
    }

    
}

