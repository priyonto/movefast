//
//  CellType.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit

/// Public enum for cell type of the collection
/// Grid is for grid layout
/// List is for list layout

public enum CellType: String {
    
    case grid = "cellGrid"
    case list = "cellList"
    
}
