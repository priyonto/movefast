//
//  GalleryListCell.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryListCell: UICollectionViewCell {
    
    // MARK:- VARIABLES DECLARATION
    
    lazy var imageView = UIImageView(cornerRadius: 10) /// Instance of UIImageView to show the image fetched from remote server withi corner radius of 10.
    lazy var containerView = UIView(color: .clear, cornerRadius: 10) /// Instances of UIView to use as container of all contents of cell
    
    /// Two instances of UIVisualEffectView created
    /// One for containing caption and resolution of image
    /// Another one for containing like button and like count
    /// Both container contains custom corner radius
    lazy var captionContainerView = UIVisualEffectView(corners: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner])
    lazy var reactionsContainerView = UIVisualEffectView(corners: [.layerMinXMaxYCorner, .layerMinXMinYCorner])
    
    /// Image caption label
    lazy var imageCaptionLabel = UILabel(text: "Caption", font: UIFont.Roboto(.Light, size: 14), color: .white, numberOfLines: 1)
    
    /// Image resolution label
    lazy var imageResolutionLabel = UILabel(text: "5400 * 2700 px", font: UIFont.Roboto(.Light, size: 12), color: .lightGray, numberOfLines: 1)
    
    /// Reach Icon ImageView
    lazy var reactIconImageView = UIImageView(cornerRadius: 0)
    /// Reach count label
    lazy var reactCountLabel = UILabel(text: "999", font: UIFont.Roboto(.Light, size: 12), color: .white)
    
    /// A instance of custom type 'Photo' that holds each photo
    /// When photo is set it updates the data of Cell immidiately
    /// Start
    
    var photo : Photo! {
        
        didSet { self.updateUI() }
    }
    
    /// End variables declaration
    
    fileprivate let bottomContentContainerHeight: CGFloat = 60 /// Constant height for bottom container that holds caption and react section
    

    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }
    
    /// This method is for setting up UI Components
    
    fileprivate func setupUI() {
        
        backgroundColor = .clear /// Setup background color
        
        /// Setup containerview as subview of superview
        /// Setup constraint to fill superview
        addSubview(containerView)
        containerView.fillSuperview()
        
        /// Setup ImageView as subview of Container View
        /// Setup constraint to fill superview
        containerView.addSubview(imageView)
        imageView.fillSuperview()
        
        /// Create a Horizontal StackView to hold caption container and reactions container
        /// Their interim space will be 16px
        /// Start
        
        let bottomContentStackView = HorizontalStackView(arrangedSubviews: [
            
            captionContainerView, reactionsContainerView
            
        ], spacing: 16)
        
        containerView.addSubview(bottomContentStackView) /// Add the stackview to container as sub view
        bottomContentStackView.constrainHeight(constant: bottomContentContainerHeight) /// Set stackview height to constant 60
        bottomContentStackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 8, right: 0)) /// Set stackview leading, trailing and bottom anchor with bottom padding
        
        /// End
    
        
        captionContainerView.layer.cornerRadius = bottomContentContainerHeight/2 /// Corner radius for caption container
        reactionsContainerView.constrainWidth(constant: 50) /// Constant width of 50 for reactions container
        reactionsContainerView.layer.cornerRadius = bottomContentContainerHeight/2 /// Corner radius for reactions container
        
        
        /// Create a Vertical StackView to hold Image Caption Label  & Image Resolution Label
        /// Their interim space will be 0px
        /// Start
        
        let imageMetaDataStackView = VerticalStackView(arrangedSubviews: [
            
            imageCaptionLabel, imageResolutionLabel
            
        ], spacing: 0)
        
        captionContainerView.contentView.addSubview(imageMetaDataStackView)
        imageResolutionLabel.constrainHeight(constant: 14) /// Set constant height of 14px for Image Resolution Label
        imageMetaDataStackView.fillSuperview(padding: .init(top: 4, left: 4, bottom: 10, right: 4))
        
        /// End
        
        
        /// Create a Vertical StackView to hold React Icon & Reaction counts label
        /// Their interim space will be 8px
        /// Start
        
        
        let reactContainerStackView = VerticalStackView(arrangedSubviews: [
            
            reactIconImageView, reactCountLabel
            
        ], spacing: 8)
        
        reactionsContainerView.contentView.addSubview(reactContainerStackView)
        

        reactIconImageView.constrainHeight(constant: 20) /// Set constant height of 20px for react icon
        reactIconImageView.contentMode = .scaleAspectFit /// Set content mode
        
        reactCountLabel.textAlignment = .center /// Set react count label text allignment to center
        reactContainerStackView.fillSuperview(padding: .init(top: 8, left: 12, bottom: 8, right: 0))
        reactContainerStackView.alignment = .center
        
        /// End
        
        
    }
    
    /// This method is for rendering data.
    
    fileprivate func updateUI() {
        
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
        imageView.sd_imageTransition = .fade
        imageView.sd_setImage(with: URL(string: photo.urls.regular))
        
        /// For caption we will try to load photo description first
        /// If photo description is nil, then try to fine alter description
        /// If alter description is also nil then show nothing as caption
        /// If caption is nil by end it will hide the caption label
        
        imageCaptionLabel.text = photo.photoDescription?.capitalized ?? photo.altDescription?.capitalized ?? ""
        
        if photo.photoDescription == nil && photo.altDescription == nil {
            
            imageCaptionLabel.alpha = 0
        
        } else {
            
            imageCaptionLabel.alpha = 1
        }
        
        
        /// Populate image resolution label
        
        imageResolutionLabel.text = "Resolution: " + photo.width.toString() + " * " + photo.height.toString() + " px"
        
        
        /// Check whether photo is liked by user
        
        if photo.likedByUser {
            
            reactIconImageView.image = #imageLiteral(resourceName: "icon_react_fill")
            
        } else {
            
            reactIconImageView.image = #imageLiteral(resourceName: "icon_react")
            
        }
        
        /// Populate reaction count
        
        reactCountLabel.text = photo.likes.toString()
    
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
