//
//  GalleryGridCell.swift
//  MoveFast
//
//  Created by Priyo on 19/3/20.
//  Copyright © 2020 Priyoneer Inc. All rights reserved.
//

import UIKit
import SDWebImage

/// Description:
/// This is the UI Collection View Cell for Grid Layout

class GalleryGridCell: UICollectionViewCell {
    
    // MARK:- VARIABLES DECLARATION
    
    lazy var imageView = UIImageView(cornerRadius: 10) /// Instance of UIImageView to show the image fetched from remote server withi corner radius of 10.
    
    /// A instance of custom type 'Photo' that holds each photo
    /// When photo is set it updates the data of Cell immidiately
    /// Start
    
    var photo : Photo! {
        
        didSet { self.updateUI() }
    }
    
    /// End
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }
    
    /// This method is for setting up UI Components
    
    fileprivate func setupUI() {
        
        backgroundColor = .clear
        addSubview(imageView)
        imageView.fillSuperview() /// Set imageView constraint to fill super view
    }
    
    /// This method is for rendering data.
    
    fileprivate func updateUI() {
        
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.white
        imageView.sd_imageTransition = .fade
        imageView.sd_setImage(with: URL(string: photo.urls.thumb))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
